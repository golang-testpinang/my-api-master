package utils

import (
	"log"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

type ValidateStruct struct {
	Name string `validate:"name"`
	// LoanTypeName string `validate:"loantypename"`
}

type ValidateTextStruct struct {
	Text string `validate:"text"`
}

type ValidateWordStruct struct {
	Word string `validate:"word"`
}

type ValidateDateStruct struct {
	DateString string `validate:"dateString"`
}

type ValidateNumericStruct struct {
	Integer string `validate:"integer"`
}

type ValidateFloatStruct struct {
	Float string `validate:"float"`
}

var validate *validator.Validate

// custom validator
func ValidateName(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[a-z A-Z,.\-]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String Name : ", fl.Field().String())
	log.Println("Result Name : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateLoanTypeName(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[a-z A-Z]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String Loan Type Name : ", fl.Field().String())
	log.Println("Result Loan Type Name : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateWord(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[a-zA-Z]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String : ", fl.Field().String())
	log.Println("Result : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateText(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[a-z A-Z]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String : ", fl.Field().String())
	log.Println("Result : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateDate(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String Date : ", fl.Field().String())
	log.Println("Result Date : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateNumeric(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[0-9]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String Numeric : ", fl.Field().String())
	log.Println("Result Numeric : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidateFloat(fl validator.FieldLevel) bool {
	regexExp := regexp.MustCompile(`^[0-9.]+$`)
	checking := regexExp.MatchString(fl.Field().String())

	log.Println("String float : ", fl.Field().String())
	log.Println("Result float : ", checking)

	if !checking {
		return false
	}

	return true
}

func ValidatorKtpNumber(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Nomor KTP tidak boleh kosong"
		errorMessageLocal = "Parameter ktpNumber kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi numeric

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: text}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Nomor KTP hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter ktpNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	intText, _ := StringInt(text)

	//validasi gagal int
	if intText == 0 {
		status = 400
		errorMessage = "Nomor KTP hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter ktpNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Nomor KTP hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter ktpNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	//validasi max min
	errsValid2 := validate.Var(text, "min=16,max=16")
	if errsValid2 != nil {
		status = 400
		errorMessage = "Nomor KTP tidak 16 digit"
		errorMessageLocal = "Format Parameter ktpNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorName(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Nama tidak boleh kosong"
		errorMessageLocal = "Parameter name kosong"

		return status, errorMessage, errorMessageLocal

	}

	//helper validator
	validate.RegisterValidation("name", ValidateName)
	str := ValidateStruct{Name: text}

	//validasi numeric
	errsValid := validate.Struct(str)
	if errsValid != nil {

		log.Println("ERR : ", errsValid.Error())

		status = 400
		errorMessage = "Nama hanya boleh menggunakan huruf"
		errorMessageLocal = "Format Parameter name tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorPlaceOfBirth(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Tempat Lahir tidak boleh kosong"
		errorMessageLocal = "Parameter placeOfBirth kosong"

		return status, errorMessage, errorMessageLocal

	}

	//helper validator
	validate.RegisterValidation("text", ValidateText)
	str := ValidateTextStruct{Text: text}

	//validasi numeric
	errsValid := validate.Struct(str)
	if errsValid != nil {

		log.Println("ERR : ", errsValid.Error())

		status = 400
		errorMessage = "Tempat Lahir hanya boleh menggunakan huruf"
		errorMessageLocal = "Format Parameter placeOfBirth tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorBirthDate(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Tanggal Lahir tidak boleh kosong"
		errorMessageLocal = "Parameter birthDate kosong"

		return status, errorMessage, errorMessageLocal

	}

	//helper validator
	validate.RegisterValidation("dateString", ValidateDate)
	str := ValidateDateStruct{DateString: text}

	//validasi numeric
	errsValid := validate.Struct(str)
	if errsValid != nil {

		log.Println("ERR : ", errsValid.Error())

		status = 400
		errorMessage = "Tanggal Lahir tidak sesuai. Hanya boleh menggunakan angka. Ex:yyyy-mm-dd"
		errorMessageLocal = "Format Parameter birthDate tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorGender(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Jenis Kelamin tidak boleh kosong"
		errorMessageLocal = "Parameter gender kosong"

		return status, errorMessage, errorMessageLocal

	}

	switch text {
	case "Laki-Laki":

		return status, errorMessage, errorMessageLocal

	case "Perempuan":

		return status, errorMessage, errorMessageLocal

	default:
		status = 400
		errorMessage = "Jenis Kelamin hanya boleh Laki-Laki atau Perempuan"
		errorMessageLocal = "Format Parameter gender tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}
}

func ValidatorMobileNumber(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Nomor Telepon tidak boleh kosong"
		errorMessageLocal = "Parameter mobileNumber kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validas prefix +62
	var isExists = strings.HasPrefix(text, "+62")
	if isExists == false {
		status = 400
		errorMessage = "Nomor Telepon tidak sesuai. Format : +628xxxxxx"
		errorMessageLocal = "Format Parameter mobileNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	//validasi numeric
	rep := strings.Replace(text, "+", "", -1)

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: rep}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Nomor Telepon tidak sesuai. Hanya boleh menggunakan angka. +628xxxxxx"
		errorMessageLocal = "Format Parameter mobileNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	intText, _ := StringInt(rep)

	if intText == 0 {
		status = 400
		errorMessage = "Nomor Telepon tidak sesuai. Hanya boleh menggunakan angka. +628xxxxxx"
		errorMessageLocal = "Format Parameter mobileNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Nomor Telepon tidak sesuai. Hanya boleh menggunakan angka. +628xxxxxx"
		errorMessageLocal = "Format Parameter mobileNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	//validasi max min
	errsValid2 := validate.Var(text, "min=12,max=14")
	if errsValid2 != nil {
		status = 400
		errorMessage = "Nomor Telepon minimal 12 digit & maksimal 14 digital"
		errorMessageLocal = "Format Parameter mobileNumber tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorEmail(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Email tidak boleh kosong"
		errorMessageLocal = "Parameter email kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi email
	errsValid := validate.Var(text, "email")
	if errsValid != nil {
		status = 400
		errorMessage = "Email tidak sesuai. Format : abc@mail.com"
		errorMessageLocal = "Format Parameter email tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorLoanTerm(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Tenor Pinjaman tidak boleh kosong"
		errorMessageLocal = "Parameter loanTerm kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi numeric

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: text}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Tenor Pinjaman hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter loanTerm tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	intText, _ := StringInt(text)

	//validasi < 0
	if intText <= 0 {
		status = 400
		errorMessage = "Tenor Pinjaman harus lebih besar dari 0"
		errorMessageLocal = "Format Parameter loanTerm tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	//validasi > 12
	if intText > 12 {
		status = 400
		errorMessage = "Tenor Pinjaman harus lebih kecil sama dengan 12"
		errorMessageLocal = "Format Parameter loanTerm tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	//validasi numeric
	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Tenor Pinjaman hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter loanTerm tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorLoanLimitEstimation(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Estimasi Nominal Pinjaman tidak boleh kosong"
		errorMessageLocal = "Parameter loanLimitEstimation kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi numeric

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: text}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Estimasi Nominal Pinjaman hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter loanLimitEstimation tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	intText, _ := StringInt(text)

	//validasi kelipatan 100.000
	if intText%100000 != 0 {
		status = 400
		errorMessage = "Pinjaman hanya dapat diajukan mulai 500.000 s/d 100 Juta dengan kelipatan 100.000."
		errorMessageLocal = "Range Parameter loanLimitEstimation tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	//validasi <= 500.000
	if intText < 500000 {
		status = 400
		errorMessage = "Pinjaman hanya dapat diajukan mulai 500.000 s/d 100 Juta."
		errorMessageLocal = "Range Parameter loanLimitEstimation tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	//validasi > 100.000.000
	if intText > 100000000 {
		status = 400
		errorMessage = "Pinjaman hanya dapat diajukan mulai 500.000 s/d 100 Juta."
		errorMessageLocal = "Range Parameter loanLimitEstimation tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Estimasi Nominal Pinjaman hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter loanLimitEstimation tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorPartnerJoinDate(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Tanggal bergabung dengan partnership tidak boleh kosong"
		errorMessageLocal = "Parameter partnerJoinDate kosong"

		return status, errorMessage, errorMessageLocal

	}

	//helper validator
	validate.RegisterValidation("dateString", ValidateDate)
	str := ValidateDateStruct{DateString: text}

	//validasi numeric
	errsValid := validate.Struct(str)
	if errsValid != nil {

		log.Println("ERR : ", errsValid.Error())

		status = 400
		errorMessage = "Tanggal bergabung dengan partnership tidak sesuai. Hanya boleh menggunakan angka. Ex:yyyy-mm-dd"
		errorMessageLocal = "Format Parameter partnerJoinDate tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorPartnershipCode(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Kode Partnership tidak boleh kosong"
		errorMessageLocal = "Parameter partnershipCode kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi min
	errsValid := validate.Var(text, "min=3")
	if errsValid != nil {
		status = 400
		errorMessage = "Kode Partnership minimal 3 digit"
		errorMessageLocal = "Format Parameter partnershipCode tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorReferenceId(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Kode pengajuan tidak boleh kosong"
		errorMessageLocal = "Parameter referenceId kosong"

		return status, errorMessage, errorMessageLocal

	}

	return 200, "", ""
}

func ValidatorAverageTransaction(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Rata-rata Nominal Transaksi 6 Bulan Terakhir tidak boleh kosong"
		errorMessageLocal = "Parameter avarageTransaction kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi numeric

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: text}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Rata-rata Nominal Transaksi 6 Bulan Terakhir hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter avarageTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	intText, _ := StringInt(text)

	//validasi <= 0
	if intText <= 0 {
		status = 400
		errorMessage = "Rata-rata Nominal Transaksi 6 Bulan Terakhir tidak boleh kurang dari 0."
		errorMessageLocal = "Range Parameter avarageTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Rata-rata Nominal Transaksi 6 Bulan Terakhir hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter avarageTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorTopTransaction(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Nominal Transaksi Tertinggi 3 Bulan Terakhir tidak boleh kosong"
		errorMessageLocal = "Parameter topTransaction kosong"

		return status, errorMessage, errorMessageLocal

	}

	//validasi numeric

	//helper validator numeric regex
	validate.RegisterValidation("integer", ValidateNumeric)
	str := ValidateNumericStruct{Integer: text}

	errsValidInt := validate.Struct(str)
	if errsValidInt != nil {

		log.Println("ERR : ", errsValidInt.Error())

		status = 400
		errorMessage = "Nominal Transaksi Tertinggi 3 Bulan Terakhir hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter topTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	intText, _ := StringInt(text)

	//validasi <= 0
	if intText <= 0 {
		status = 400
		errorMessage = "Nominal Transaksi Tertinggi 3 Bulan Terakhir tidak boleh kurang dari 0."
		errorMessageLocal = "Range Parameter topTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}

	errsValid := validate.Var(intText, "numeric")
	if errsValid != nil {
		status = 400
		errorMessage = "Nominal Transaksi Tertinggi 3 Bulan Terakhir hanya boleh menggunakan angka"
		errorMessageLocal = "Format Parameter topTransaction tidak sesuai"

		return status, errorMessage, errorMessageLocal

	}

	return status, errorMessage, errorMessageLocal
}

func ValidatorLoanTypeCode(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Kode Tipe Pinjaman tidak boleh kosong"
		errorMessageLocal = "Parameter loanTypeCode kosong"

		return status, errorMessage, errorMessageLocal

	}

	switch text {
	case "40002": // tetap

		return status, errorMessage, errorMessageLocal

	case "40003": // transactional

		return status, errorMessage, errorMessageLocal

	default:
		status = 400
		errorMessage = "Kode Tipe Pinjaman hanya boleh Laki-Laki atau Perempuan"
		errorMessageLocal = "Format Parameter loanTypeCode tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}
}

func ValidatorMaritalStatus(text string) (status int, errorMessage string, errorMessageLocal string) {
	status = 200

	validate = validator.New()

	if text == "" {
		status = 400
		errorMessage = "Status Perkawinan tidak boleh kosong"
		errorMessageLocal = "Parameter maritalStatus kosong"

		return status, errorMessage, errorMessageLocal

	}

	switch text {
	case "Kawin":

		return status, errorMessage, errorMessageLocal

	case "Belum Kawin":

		return status, errorMessage, errorMessageLocal

	case "Cerai Hidup":

		return status, errorMessage, errorMessageLocal

	case "Cerai Mati":

		return status, errorMessage, errorMessageLocal

	default:
		status = 400
		errorMessage = "Status Perkawinan hanya boleh Kawin, Belum Kawin, Cerai Hidup atau Cerai Mati"
		errorMessageLocal = "Format Parameter maritalStatus tidak sesuai"

		return status, errorMessage, errorMessageLocal
	}
}
