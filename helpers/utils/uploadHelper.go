package utils

import (
	"bytes"
	"encoding/base64"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"strings"
	"sync"

	Config "gitlab.com/my-api-master/configs"
)

var mediaPath, _ = Config.MediaPath()

func UploadFile(wg *sync.WaitGroup, fileString string, mimeType string, path string, fileName string) (status int, finalPath string, messages string) {
	defer wg.Done()

	unbased, _ := base64.StdEncoding.DecodeString(string(fileString)) //decode base64
	stringFile := bytes.NewReader(unbased)

	var pathFileName string

	pathUploadJoin := []string{mediaPath, "/", path}
	pathUpload := strings.Join(pathUploadJoin, "")

	os.MkdirAll(pathUpload, os.ModePerm) //create folder in media

	if err := os.Chmod(pathUpload, 0777); err != nil {
		log.Println("Heplers --- Error Chmod ---")
	}

	// log.Println(path)
	// log.Println(mimeType)

	switch mimeType {
	case "image/jpeg": //convert to jpg
		pathFileNameJoin := []string{pathUpload, "/", fileName, ".jpg"}
		pathMove := strings.Join(pathFileNameJoin, "")

		originalPathJoin := []string{path, "/", fileName, ".jpg"}
		pathFileName = strings.Join(originalPathJoin, "")

		jpgImage, errJpg := jpeg.Decode(stringFile)

		f, errUpload := os.OpenFile(pathMove, os.O_WRONLY|os.O_CREATE, 0777)

		if errUpload != nil {
			log.Println("Heplers --- Error Image Created JPG ---")
			return 400, "", errJpg.Error()
		}

		jpeg.Encode(f, jpgImage, &jpeg.Options{Quality: jpeg.DefaultQuality})
		log.Println("Heplers --- Image Created JPG ---")

		defer func() {
			f.Close()
		}()
	case "image/png": //convert to png
		pathFileNameJoin := []string{pathUpload, "/", fileName, ".png"}
		pathMove := strings.Join(pathFileNameJoin, "")

		originalPathJoin := []string{path, "/", fileName, ".png"}
		pathFileName = strings.Join(originalPathJoin, "")

		pngImage, errPng := png.Decode(stringFile)

		if errPng != nil {
			log.Println("Heplers --- Error Decode Image Created PNG ---")
			return 400, "", errPng.Error()
		}

		f, errUpload := os.OpenFile(pathMove, os.O_WRONLY|os.O_CREATE, 0777)

		if errUpload != nil {
			log.Println("Heplers --- Error Image Created PNG ---")
			return 400, "", errPng.Error()
		}

		png.Encode(f, pngImage)
		log.Println("Heplers --- Image Created PNG ---")

		defer func() {
			f.Close()
		}()
	case "application/pdf": //convert to pdf
		pathFileNameJoin := []string{pathUpload, "/", fileName, ".pdf"}
		pathMove := strings.Join(pathFileNameJoin, "")

		originalPathJoin := []string{path, "/", fileName, ".pdf"}
		pathFileName = strings.Join(originalPathJoin, "")

		f, errUpload := os.OpenFile(pathMove, os.O_WRONLY|os.O_CREATE, 0777)

		if errUpload != nil {
			log.Println("Heplers --- Error Image Created PDF ---")
			return 400, "", errUpload.Error()
		}

		_, errWrite := f.Write(unbased)

		if errWrite != nil {
			if errUpload != nil {
				log.Println("Heplers --- Error Image Created PDF ---")
				return 400, "", errWrite.Error()
			}
		}

		log.Println("Heplers --- Image Created PDF ---")

		defer func() {
			f.Close()
		}()
	}

	// defer wg.Done()

	return 200, pathFileName, "File Uploaded"

}
