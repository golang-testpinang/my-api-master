package utils

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	config "gitlab.com/my-api-master/configs"
	ct "gitlab.com/my-api-master/helpers/constants"
)

type V1LoginSSOCurlParams struct {
	UserId    string
	Password  string
	IpAddress string
}

type V1SSOCurlResponse struct {
	Status       int
	Message      string
	MessageLocal string
}

type V1LoginSSOCurlMapping struct {
	StatusCode        string             `json:"statusCode"`
	StatusDescription string             `json:"statusDescription"`
	Data              V1DataLoginCurlSSO `json:"data"`
}

type V1DataLoginCurlSSO struct {
	LoginTicket string `json:"loginTicket"`
	Id          string `json:"userId"`
	Nama        string `json:"userName"`
	Branch      string `json:"branchCode"`
	OrgId       string `json:"orgId"`
	Organisasi  string `json:"organization"`
	Jabatan     string `json:"jabatan"`
	Foto        string `json:"foto"`
}

type V1LogoutSSOCurlParams struct {
	UserId      string
	LoginTicket string
}
type V1LogoutCurlSSOMapping struct {
	StatusCode        string `json:"statusCode"`
	StatusDescription string `json:"statusDescription"`
}

type V1EmployeeDetailCurlParams struct {
	PersonalNumber string
}
type V1EmployeeDetailCurlResponse struct {
	Status       int
	Message      string
	MessageLocal string
}
type V1EmployeeDetailCurlMapping struct {
	StatusCode        string                    `json:"statusCode"`
	StatusDescription string                    `json:"statusDescription"`
	Data              V1DataEmbedEmployeeDetail `json:"data"`
}
type V1DataEmbedEmployeeDetail struct {
	Id             string `json:"userId"`
	Name           string `json:"userName"`
	BranchCode     string `json:"branchCode"`
	BranchAddress  string `json:"branchAddress"`
	OrganizationId string `json:"orgId"`
	Organization   string `json:"organization"`
	Position       string `json:"jabatan"`
	Foto           string `json:"foto"`
}

func CurlLoginSSOV1(params V1LoginSSOCurlParams) (response V1SSOCurlResponse, data V1LoginSSOCurlMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	var paramsCurl = url.Values{}
	paramsCurl.Set("userid", params.UserId)
	paramsCurl.Set("password", params.Password)
	paramsCurl.Set("ip_address", params.Password)

	log.Println("Helper -- PARAMS CURL LOGIN--")
	log.Println(paramsCurl)

	endpoint, _ := config.HostSSO("loginsso")

	appEnv, _ := config.AppEnv()
	log.Println("--APP ENV : ", appEnv)

	if appEnv == "STAGING" {
		endpoint += "/staging"
	}

	// usernameSSO, _ := ct.UserSSO()
	// passwordSSO, _ := ct.PassSSO()
	log.Println("--ENDPOINT SSO LOGIN--")
	log.Println(endpoint)
	// fmt.Println(usernameSSO)
	// fmt.Println(passwordSSO)

	transCfg := ct.TransportConfig

	client := &http.Client{
		Timeout:   time.Duration(30 * time.Second),
		Transport: transCfg,
	}

	r, err := http.NewRequest("POST", endpoint, strings.NewReader(paramsCurl.Encode())) // URL-encoded payload

	//ERROR REQUEST
	if err != nil {
		log.Println("Helper -- ERROR REQUEST CURL LOGIN ---")
		log.Println("Helper -- ERROR REQUEST CURL LOGIN : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	r.Header.Set(ct.CURLHeaderContentType, ct.CURLHeaderContentTypeValue)
	r.Header.Set(ct.CURLHeaderCacheControl, ct.CURLHeaderCacheControlValue)
	// r.SetBasicAuth(usernameSSO, passwordSSO)
	curlResponse, err := client.Do(r)

	//ERROR CURL
	if err != nil {
		log.Println("Helper -- ERROR HIT API CURL LOGIN ---")
		log.Println("Helper -- ERROR HIT API CURL LOGIN : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	defer curlResponse.Body.Close()

	log.Println("Helper -- RESPONSE STATUS CURL LOGIN : ", curlResponse.Status)
	log.Println("Helper -- RESPONSE HEADERS CURL LOGIN : ", curlResponse.Header)
	log.Println("Helper -- REQUEST URL CURL LOGIN : ", curlResponse.Request.URL)
	log.Println("Helper -- REQUEST CONTENT LENGTH CURL LOGIN : ", curlResponse.Request.ContentLength)

	//ERROR STATUS CODE
	if curlResponse.StatusCode != 200 {
		// log.Println("Helper --- ERROR STATUS CODE CURL LOGIN ---")
		// log.Println("Helper --- ERROR STATUS CODE CURL LOGIN : ", curlResponse.StatusCode)

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	json.NewDecoder(curlResponse.Body).Decode(&data)

	// log.Println("Helper --- DATA DECODE CURL LOGIN ---")
	// log.Println(data)

	if data.StatusCode == "99" {
		response := V1SSOCurlResponse{
			Status:       401,
			Message:      data.StatusDescription,
			MessageLocal: data.StatusDescription,
		}

		return response, data
	}

	response = V1SSOCurlResponse{
		Status:       200,
		Message:      "Login berhasil",
		MessageLocal: "Login berhasil",
	}

	return response, data
}

func CurlLogoutSSOV1(params V1LogoutSSOCurlParams) (response V1SSOCurlResponse, data V1LogoutCurlSSOMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	var paramsCurl = url.Values{}
	paramsCurl.Set("userid", params.UserId)
	paramsCurl.Set("loginTicket", params.LoginTicket)

	log.Println("Helper -- PARAMS CURL LOGOUT--")
	log.Println(paramsCurl)

	endpoint, _ := config.HostSSO("logoutsso")

	appEnv, _ := config.AppEnv()
	log.Println("--APP ENV : ", appEnv)

	if appEnv == "STAGING" {
		endpoint += "/staging"
	}

	// usernameSSO, _ := ct.UserSSO()
	// passwordSSO, _ := ct.PassSSO()
	log.Println("Helper -- ENDPOINT SSO LOGOUT --")
	log.Println(endpoint)
	// log.Println(usernameSSO)
	// log.Println(passwordSSO)

	transCfg := ct.TransportConfig

	client := &http.Client{
		Timeout:   time.Duration(30 * time.Second),
		Transport: transCfg,
	}

	r, err := http.NewRequest("POST", endpoint, strings.NewReader(paramsCurl.Encode())) // URL-encoded payload

	//ERROR REQUEST
	if err != nil {
		log.Println("Helper -- ERROR REQUEST CURL LOGOUT --")
		log.Println("Helper -- ERROR REQUEST CURL LOGOUT : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	r.Header.Set(ct.CURLHeaderContentType, ct.CURLHeaderContentTypeValue)
	r.Header.Set(ct.CURLHeaderCacheControl, ct.CURLHeaderCacheControlValue)
	// r.SetBasicAuth(usernameSSO, passwordSSO)
	curlResponse, err := client.Do(r)

	//ERROR CURL
	if err != nil {
		log.Println("Helper -- ERROR HIT API CURL LOGOUT --")
		log.Println("Helper -- ERROR HIT API CURL LOGOUT : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	defer curlResponse.Body.Close()

	log.Println("Helper -- RESPONSE STATUS CURL LOGOUT : ", curlResponse.Status)
	log.Println("Helper -- RESPONSE HEADERS CURL LOGOUT : ", curlResponse.Header)
	log.Println("Helper -- REQUEST URL CURL LOGOUT : ", curlResponse.Request.URL)
	log.Println("Helper -- REQUEST CONTENT LENGTH CURL LOGOUT : ", curlResponse.Request.ContentLength)

	//ERROR STATUS CODE
	if curlResponse.StatusCode != 200 {
		// log.Println("Helper -- ERROR STATUS CODE CURL LOGOUT ---")
		// log.Println("Helper -- ERROR STATUS CODE CURL LOGOUT : ", curlResponse.StatusCode)

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1SSOCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	json.NewDecoder(curlResponse.Body).Decode(&data)

	// log.Println("Helper -- DATA DECODE ---")
	// log.Println(data)

	if data.StatusCode != "1" {
		response := V1SSOCurlResponse{
			Status:       400,
			Message:      "Logout gagal",
			MessageLocal: "Logout gagal",
		}

		return response, data
	}

	response = V1SSOCurlResponse{
		Status:       200,
		Message:      "Logout berhasil",
		MessageLocal: "Logout berhasil",
	}

	return response, data
}

func CurlEmployeeDetailV1(params V1EmployeeDetailCurlParams) (response V1EmployeeDetailCurlResponse, data V1EmployeeDetailCurlMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	firstEndpoint, _ := config.APIDataAgri()
	queryStringJoin := []string{firstEndpoint, "/api/digiagri/employee/inquiry/", params.PersonalNumber}
	endpoint := strings.Join(queryStringJoin, "")

	log.Println("--ENDPOINT API EMPLOYEE DETAIL--")
	log.Println(endpoint)

	transCfg := ct.TransportConfig

	client := &http.Client{
		Timeout:   time.Duration(25 * time.Second),
		Transport: transCfg,
	}

	r, err := http.NewRequest("GET", endpoint, nil) // URL-encoded payload

	//ERROR REQUEST
	if err != nil {
		log.Println("Helper -- ERROR REQUEST CURL EMPLOYEE DETAIl ---")
		log.Println("Helper -- ERROR REQUEST CURL EMPLOYEE DETAIl : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1EmployeeDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	r.Header.Set(ct.CURLHeaderContentType, ct.CURLHeaderContentTypeValue)
	r.Header.Set(ct.CURLHeaderCacheControl, ct.CURLHeaderCacheControlValue)
	// r.SetBasicAuth(usernameSSO, passwordSSO)
	curlResponse, err := client.Do(r)

	//ERROR CURL
	if err != nil {
		log.Println("Helper -- ERROR HIT API CURL EMPLOYEE DETAIl ---")
		log.Println("Helper -- ERROR HIT API CURL EMPLOYEE DETAIl : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1EmployeeDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	defer curlResponse.Body.Close()

	log.Println("Helper -- RESPONSE STATUS CURL EMPLOYEE DETAIL : ", curlResponse.Status)
	log.Println("Helper -- RESPONSE HEADERS CURL EMPLOYEE DETAIL : ", curlResponse.Header)
	log.Println("Helper -- REQUEST URL CURL EMPLOYEE DETAIL : ", curlResponse.Request.URL)
	log.Println("Helper -- REQUEST CONTENT LENGTH CURL EMPLOYEE DETAIL : ", curlResponse.Request.ContentLength)

	//ERROR STATUS CODE
	if curlResponse.StatusCode != 200 {
		// log.Println("Helper --- ERROR STATUS CODE CURL EMPLOYEE DETAIl ---")
		// log.Println("Helper --- ERROR STATUS CODE CURL EMPLOYEE DETAIl : ", curlResponse.StatusCode)

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1EmployeeDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	json.NewDecoder(curlResponse.Body).Decode(&data)

	// log.Println("Helper --- DATA DECODE CURL EMPLOYEE DETAIl ---")
	// log.Println(data)

	if data.StatusCode == "99" {
		response := V1EmployeeDetailCurlResponse{
			Status:       401,
			Message:      data.StatusDescription,
			MessageLocal: data.StatusDescription,
		}

		return response, data
	}

	response = V1EmployeeDetailCurlResponse{
		Status:       200,
		Message:      "Data employee berhasil ditemukan",
		MessageLocal: "Employee Found",
	}

	return response, data
}
