package jwt

import (
	"context"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	config "gitlab.com/my-api-master/configs"
	cc "gitlab.com/my-api-master/helpers/constants"
)

type Login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type User struct {
	UserName string
}

func Middleware() *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:      cc.AppJWT,
		Key:        []byte(cc.KeyJWT),
		Timeout:    168 * time.Hour,
		MaxRefresh: 168 * time.Hour,

		IdentityKey: cc.IdentityJWT,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*User); ok {
				return jwt.MapClaims{
					cc.IdentityJWT: v.UserName,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &User{
				UserName: claims[cc.IdentityJWT].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals Login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			userID := loginVals.Username
			password := loginVals.Password

			statusAuth, _ := config.GetAuth(userID, password, "FALSE")

			if statusAuth == 200 {
				return &User{
					UserName: userID,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			statusAuth, _ := config.GetAuth(data.(*User).UserName, "", "TRUE")

			if statusAuth == 200 {
				return true
			}

			log.Println("---USER NOT MATCH JWT---")

			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			log.Println("---UNAUTORIZED JWT---")

			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},

		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	return authMiddleware
}

/*
Check Token
*/
func TokenAuthentication() gin.HandlerFunc {
	return func(c *gin.Context) {
		authToken := c.Request.Header.Get("Authorization")
		log.Println("Auth Token : ", authToken)

		if authToken == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"code":    403,
				"message": "403 Forbidden",
				"desc":    "Tidak Memliki Akses",
			})

			return
		}

		token := strings.TrimPrefix(authToken, "Bearer ")
		log.Println("Token : ", token)

		ctx := context.Background()
		redisClient := config.InitRedisConnection(config.RedisDBToken())

		checkToken, err := redisClient.Get(ctx, token).Result()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Invalid Token || Token Is Expired",
				"desc":    "Expired",
			})

			return
		}

		log.Println("CHECK TOKEN : ", checkToken)

		if checkToken == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Invalid Token || Token Is Expired",
				"desc":    "Expired",
			})

			return
		}

		c.Next()
	}
}
