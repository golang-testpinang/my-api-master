package models

import (
	"errors"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	config "gitlab.com/my-api-master/configs"
	ct "gitlab.com/my-api-master/helpers/constants"
	Helpers "gitlab.com/my-api-master/helpers/utils"
	"gorm.io/gorm"
)

type GormMasterRepository struct {
	database *gorm.DB
}

func NewRepositoryMaster(db *gorm.DB) *GormMasterRepository {
	return &GormMasterRepository{
		database: db,
	}
}

type Repositories interface {
	GetBranch(params BranchParams) (response BranchResponse, data []BranchMapping)
}

type Branch struct {
	gorm.Model
	ID                 uint   `gorm:"primaryKey;AUTO_INCREMENT;column:id"`
	Code               string `gorm:"column:code"`
	CorrespondanceCode string `gorm:"column:correspondance_code"`
	Name               string `gorm:"column:name"`
	Address            string `gorm:"column:address"`
	Phone              string `gorm:"column:phone"`
	Level              string `gorm:"column:level"`
	Head               string `gorm:"column:head"`
	Status             int    `gorm:"column:status"`

	CreatedBy *string    `gorm:"column:created_by"`
	UpdatedBy *string    `gorm:"column:updated_by"`
	DeletedBy *string    `gorm:"column:deleted_by"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
}

func (Branch) TableName() string {
	return "branch"
}

type BranchParams struct {
	Id          int
	KtpNumber   string
	StartIndex  string
	RecordCount string
	UserId      string
	UserIds     []string
	TextSearch  string
	StatusType  []int
	ExcludeId   []int
}

type BranchResponse struct {
	Status       int
	Message      string
	MessageLocal string
}

type BranchMapping struct {
	ID                 int    `json:"id"`
	Code               string `json:"code"`
	CorrespondanceCode string `json:"correspondance_code"`
	Name               string `json:"name"`
	Address            string `json:"address"`
	Phone              string `json:"phone"`
	Level              string `json:"level"`
	Head               string `json:"head"`
	Status             int    `json:"status"`

	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

func (repo *GormMasterRepository) GetBranch(params BranchParams) (response BranchResponse, data []BranchMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	response = BranchResponse{
		Status:       400,
		Message:      ct.ErorrGeneralMessage,
		MessageLocal: ct.ErorrDBDefaultMessageLocal,
	}

	/* open connection */
	db, err := config.InitDb()
	if err != nil {
		response = BranchResponse{
			Status:       400,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: err.Error(),
		}

		return response, data
	}
	/* end open connection */

	/* build query */
	var result []Branch

	query := db.Debug().Order("name asc")
	// Where("code != ?", "9999")

	if params.TextSearch != "" {
		stringSearch := "%"
		stringSearch += params.TextSearch
		stringSearch += "%"
		query = query.Where("name LIKE ? OR code LIKE ?", stringSearch, stringSearch)
	}

	startIndex, _ := Helpers.StringInt(params.StartIndex)
	recordCount, _ := Helpers.StringInt(params.RecordCount)

	query.Limit(recordCount).Offset(startIndex).Find(&result)
	/* end query */

	/* error handling */
	if query.Error != nil { /*error query*/
		if query.Statement.Error != nil {
			response = BranchResponse{
				Status:       500,
				Message:      "Cabang Tidak Ditemukan",
				MessageLocal: query.Statement.Error.Error(),
			}

			log.Println("Model -- ERROR QUERY STATMENT BRANCH LIST --")
			log.Println("Model -- ERROR QUERY STATMENT BRANCH LIST : ", query.Statement.Error.Error())
		}

		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			response = BranchResponse{
				Status:       500,
				Message:      "Cabang Tidak Ditemukan",
				MessageLocal: ct.ErorrDBRecordNotFoundMessageLocal,
			}

			log.Println("Model -- DATA NOT FOUND BRANCH LIST --")
		}

		return response, data
	}

	if len(result) < 1 { /*data empty*/
		response = BranchResponse{
			Status:       404,
			Message:      "Cabang Tidak Ditemukan",
			MessageLocal: ct.ErorrDBRecordNotFoundMessageLocal,
		}

		log.Println("Model -- DATA EMPTY BRANCH LIST --")

		return response, data
	}
	/* end error handling */

	/* mapping data to struct */
	for _, v := range result {
		var allData = BranchMapping{
			ID:                 int(v.ID),
			Code:               v.Code,
			CorrespondanceCode: v.CorrespondanceCode,
			Name:               v.Name,
			Address:            v.Address,
			Phone:              v.Phone,
			Level:              v.Level,
			Head:               v.Head,
			Status:             v.Status,
			CreatedAt:          v.CreatedAt,
			UpdatedAt:          v.UpdatedAt,
			DeletedAt:          v.DeletedAt,
		}

		data = append(data, allData) /*set result with data*/
	}
	/* end mapping data to struct */

	log.Println("Model -- RESPONSE MODEL BRANCH LIST --")
	// log.Println(data)
	// return

	response = BranchResponse{
		Status:       200,
		Message:      ct.DBDataDitemukanMessage,
		MessageLocal: ct.DBDataValidMessageLocal,
	}
	return response, data
}
