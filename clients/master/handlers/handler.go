package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	client "gitlab.com/my-api-master/clients/master/usecases"
	"gitlab.com/my-api-master/helpers/constants"
	pb "gitlab.com/my-api-master/proto/master"
)

func HandlerBranchList(c *gin.Context) {

	userId := c.DefaultPostForm("userId", "")
	textSearch := c.DefaultPostForm("textSearch", "")
	startIndex := c.DefaultPostForm("startIndex", "0")
	recordCount := c.DefaultPostForm("recordCount", "1000")

	req := &pb.BranchListRequest{
		UserId:      userId,
		TextSearch:  textSearch,
		StartIndex:  startIndex,
		RecordCount: recordCount,
	}
	clientOpen := client.OpenServer()
	if response, err := clientOpen.MasterBranchList(c, req); err == nil {
		if response.GetStatus() == 400 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  500,
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    nil,
			})

			return
		}

		if response.GetStatus() != 200 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    nil,
			})

			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    response.EmbedDataBranchLists,
			})
			return
		}

	} else {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  500,
			"message": constants.ErorrGeneralMessage,
			"desc":    err.Error(),
			"data":    nil,
		})
	}
}

func HandlerBranchDetail(c *gin.Context) {
	branchCode := c.Param("branchId")

	req := &pb.BranchDetailRequest{
		BranchId: branchCode,
	}
	clientOpen := client.OpenServer()
	if response, err := clientOpen.MasterBranchDetail(c, req); err == nil {
		if response.GetStatus() == 400 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  500,
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    nil,
			})

			return
		}

		if response.GetStatus() != 200 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    nil,
			})
			return

		} else {
			c.JSON(http.StatusOK, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetMessageLocal(),
				"data":    response.EmbedDataBranchDetails,
			})
			return
		}

	} else {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  500,
			"message": constants.ErorrGeneralMessage,
			"desc":    err.Error(),
			"data":    nil,
		})
	}
}
