package usecases

import (
	"log"
	"os"
	"time"

	pb "gitlab.com/my-api-master/proto/master"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

/*
Open Server
*/
func OpenServer() pb.MasterServiceClient {
	portMain := os.Getenv("PORT_CLIENT_MASTER")
	addressMainV1 := os.Getenv("PORT_SERVICE_MASTER")

	maxMsgSize := 1024 * 1024 * 20

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(maxMsgSize),
		grpc.MaxCallSendMsgSize(maxMsgSize),
	))
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))

	connectionService, err := grpc.Dial(
		addressMainV1,
		opts...,
	)

	if err != nil {
		log.Printf("did not connect: %v\n", err)
	}

	log.Println("START OPEN SERVER MASTER IN ", portMain, " HIT : ", time.Now())
	log.Println("RUN SERVICE MASTER IN ", addressMainV1)

	clientOpen := pb.NewMasterServiceClient(connectionService)
	return clientOpen
}
