package main

import (
	"io"
	"log"
	"net"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/my-api-master/configs"
	"gitlab.com/my-api-master/models"
	"gitlab.com/my-api-master/server/master/usecases"
)

func main() {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	port := os.Getenv("PORT_SERVICE_MASTER")

	log.Println("--- START SERVICE MASTER V1 --- ", "PORT", port)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Println("Failed to listen : ", err)
	}

	db, err := configs.InitDb()
	if err != nil {
		return
	}
	repo := models.NewRepositoryMaster(db)
	srv := usecases.NewRPCServer(repo)

	if err := srv.Serve(lis); err != nil {
		log.Println("Failed to server : ", err)
	}

}
